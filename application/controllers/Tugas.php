<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tugas extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->login_model->logged_id()){
			redirect('Auth','refresh');
		}
	}
	
	public function index()	{
		$data['page'] = 'tugas/add';
		$data['sidebar'] = $this->session->userdata['_type'];
		$sql = "SELECT tipe_mobil FROM data_stok GROUP BY tipe_mobil ORDER BY tipe_mobil ASC";
		$data['list_tipe_mobil'] = $this->db->query($sql);

		$data['list'] = $this->db->where('stok', 1);
		$data['list'] = $this->db->get('data_stok');
		
		$this->load->view('_partials/template', $data);
	}

	public function buattugas() {
		if (isset($_POST)) {
			$var = $this->session->userdata;

			$_POST['id_pic'] = $var['_user_id'];
			if ($_POST['jam_kembali'] == '00:00:00') {
				$_POST['jam_kembali'] = null;
			}
			$add = $this->db->insert('sk_keluar', $_POST);
			if($add) {
				$id = $this->db->insert_id();
				$notif['dari'] = $this->session->userdata['_name'];
				$notif['pesan'] = 'Ada penugasan pembuatan SK Keluar dari '.$this->session->userdata['_name'];
				$notif['tipe'] = 'info';

				$this->db->where('type', 'admin');
				$getAdmin = $this->db->get('users');
				foreach ($getAdmin->result() as $pic):
					$notif['untuk'] = $pic->id;
					$this->db->insert('user_notifikasi', $notif);
				endforeach;

				$this->session->set_flashdata('success', "PENGAJUAN SK KELUAR BERHASIL DIBUAT");
			} else {
				$this->session->set_flashdata('error', "GAGAL MEMBUAT PENGAJUAN SK KELUAR");
			}
		}

		echo '<script type="text/javascript">
				    window.location.href="'.$_SERVER['HTTP_REFERER'].'";
				</script>';
	}

	public function warnaMobil($tipe_mobil)	{
		$tipe_mobil = str_replace('%20',' ',$tipe_mobil);
		$sql = "SELECT warna_mobil FROM data_stok WHERE tipe_mobil = '".$tipe_mobil."' GROUP BY warna_mobil ORDER BY warna_mobil ASC";
		$list_warna_mobil = $this->db->query($sql);
		$result = '';
		foreach ($list_warna_mobil->result() as $data):
			$result .= "<option value='".$data->warna_mobil."'>".$data->warna_mobil."</option>";
		endforeach;
		print_r($result);
	}
}