<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifikasi extends CI_Controller {
	public function index($id_notifikasi)	{
		$this->db->where('id_notifikasi', $id_notifikasi);
		$data['isRead'] = 1;
		$this->db->update('user_notifikasi', $data);
		echo '<script type="text/javascript">
				    window.location.href="'.$_SERVER['HTTP_REFERER'].'";
				</script>';
	}
}