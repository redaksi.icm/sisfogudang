<?php if ($this->session->flashdata('success')): ?>
<div class="alert alert-success" role="alert">
	<?php echo $this->session->flashdata('success'); ?>
</div>
<?php endif; ?>
<?php if ($this->session->flashdata('error')): ?>
<div class="alert alert-error" role="alert">
	<?php echo $this->session->flashdata('error'); ?>
</div>
<?php endif; ?>

<div class="card mb-3">
    <div class="card-body">
        <form method="post" action="<?php echo site_url('skkeluar/buatsk/').$sk_keluar[0]['id_sk_keluar']; ?>">
            <div class="form-group">
                <label for="name">Tipe Mobil</label>
                <input type="text" name="tipe_mobil" class="form-control"  value="<?php echo $sk_keluar[0]['tipe_mobil']; ?>" readonly/>
            </div>
            <div class="form-group">
                <label for="name">Warna Mobil</label>
                <input type="text" name="warna_mobil" class="form-control"  value="<?php echo $sk_keluar[0]['warna_mobil']; ?>" readonly/>
            </div>
            <div class="form-group">
                <label for="name">Nomor Rangka*</label>
                <select name="nomor_rangka" id="nomor_rangka" class="form-control" onchange="getWarnaMobil()" required>
                    <option value="" disabled selected>PILIH NOMOR RANGKA</option>
                    <?php foreach ($list_mobil->result() as $mobil): ?>
                        <option value="<?php echo $mobil->nomor_rangka; ?>"><?php echo $mobil->nomor_rangka; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Peruntukan</label>
                <input type="text" name="peruntukan" class="form-control"  value="<?php echo $sk_keluar[0]['peruntukan']; ?>" readonly/>
            </div>
            <div class="form-group">
                <label for="name">Permintaan Dari</label>
                <input type="text" name="permintaan_dari" class="form-control"  value="<?php echo $sk_keluar[0]['permintaan_dari']; ?>" readonly/>
            </div>
            <div class="form-group">
                <label for="name">Tanggal</label>
                <input type="text" name="tanggal" class="form-control"  value="<?php echo $sk_keluar[0]['tanggal']; ?>" readonly/>
            </div>
            <div class="form-group">
                <label for="name">Pembawa (Driver)*</label>
                <select name="pembawa" id="pembawa" class="form-control" required>
                    <option value="" disabled selected>PILIH PEMBAWA / DRIVER</option>
                    <?php foreach ($list_driver as $driver): ?>
                        <option value="<?php echo $driver->id; ?>"><?php echo $driver->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Jam Keluar</label>
                <input type="text" name="jam_keluar" class="form-control"  value="<?php echo $sk_keluar[0]['jam_keluar']; ?>" readonly/>
            </div>
            <div class="form-group">
                <label for="name">Jam Kembali</label>
                <input type="text" name="jam_kembali" class="form-control"  value="<?php echo $sk_keluar[0]['jam_kembali']; ?>" readonly/>
            </div>
            <div class="form-group">
                <label for="name">Keterangan</label>
                <input type="text" name="keterangan" id="keterangan"value="<?php echo $sk_keluar[0]['keterangan']; ?>" class="form-control"/>
            </div>
            <input class="btn btn-success" type="submit" value="BUAT & KIRIM SK">
            <a href="#" onClick="tolakSK()" class="btn btn-danger" style="float: right;">TOLAK PENGAJUAN SK</a>
        </form>
    </div>
</div>

<div class="card-footer small text-muted">
	* Harus Diisi
</div>

<script>
    function tolakSK() {
      var keterangan = document.getElementById('keterangan').value;
      if (keterangan === '') {
          keterangan = '-'
      }
      var url = '<?php echo site_url('skkeluar/tolaksk/').$sk_keluar[0]['id_sk_keluar'];?>/'+keterangan+'/id_pic';
      window.location.href = url;
    }
</script>