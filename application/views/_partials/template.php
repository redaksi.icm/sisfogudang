<!DOCTYPE html>
	<html lang="en" >
	<head>
		<?php $this->load->view("_partials/head.php") ?>
	</head>
	<body id="page-top">
		<!-- Page Wrapper -->
  	<div id="wrapper">

      <!-- Sidebar -->
			<?php
				if ($sidebar == 'pic') {
					$this->load->view("_partials/sidebar_pic.php"); 
				} elseif ($sidebar == 'security') {
					$this->load->view("_partials/sidebar_security.php");
				} elseif ($sidebar == 'admin') {
					$this->load->view("_partials/sidebar_admin.php");
				}
			?>
      <!-- End of Sidebar -->

	  	<!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">
	  
	        <!-- Topbar -->
	        <?php $this->load->view("_partials/topbar.php") ?>
	        <!-- End of Topbar -->

	        <!-- Begin Page Content -->
	        <div class="container-fluid">
		        <?php $this->load->view($page); ?>
	        </div>
		      <!-- /.container-fluid -->
		
		    </div>
		    <!-- End of Main Content -->
		
		    <!-- Footer -->
		    <?php $this->load->view("_partials/footer.php") ?>
		    <!-- End of Footer -->

	    </div>
      <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
		<?php $this->load->view("_partials/scrolltop.php") ?>
	
		<!-- Modal-->
		<?php $this->load->view("_partials/modal.php") ?>

		<!-- Load JS-->
		<?php $this->load->view("_partials/js.php") ?>
