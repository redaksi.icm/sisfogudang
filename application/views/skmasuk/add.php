<?php if ($this->session->flashdata('success')): ?>
<div class="alert alert-success" role="alert">
	<?php echo $this->session->flashdata('success'); ?>
</div>
<?php endif; ?>
<?php if ($this->session->flashdata('error')): ?>
<div class="alert alert-danger" role="alert">
	<?php echo $this->session->flashdata('error'); ?>
</div>
<?php endif; ?>

<div class="card mb-3">
    <div class="card-body">
        <form method="post" action="<?php echo site_url('skmasuk/buatskmasuk')?> ">
            <div class="form-group">
            <label for="name">Tipe Mobil*</label>
                <select name="tipe_mobil" id="tipe_mobil" class="form-control" onchange="getWarnaMobil()" required>
                    <option value="" disabled selected>PILIH TIPE MOBIL</option>
                    <?php foreach ($list_tipe_mobil->result() as $data): ?>
                        <option value="<?php echo $data->tipe_mobil; ?>"><?php echo $data->tipe_mobil; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
              <label for="name">Warna Mobil*</label>
                <select name="warna_mobil" id="warna_mobil" class="form-control"required>
                    <option value="" disabled selected>PILIH WARNA MOBIL</option>
                </select>
            </div>
            <div class="form-group">
              <label for="name">No. Rangka*</label>
                <input class="form-control" required type="text" name="nomor_rangka" id="nomor_rangka" onKeyUp = "cekNomorRangka()" />
            </div>
            <div class="form-group">
              <label for="name">Mobil Dari*</label>
              <select name="asal_mobil" class="form-control">
                <option value="Main Dealer">Main Dealer</option>
                <option value="Dealer">Dealer</option>
                <option value="Cabang Lain">Cabang Lain</option>
              </select>
            </div>
            <div class="form-group">
                <label for="name">Tanggal*</label>
                <input class="form-control" required type="date" name="tanggal" value="<?php echo date('Y-m-d'); ?>"/>
            </div>
            <div class="form-group">
                <label for="name">Pembawa (Driver)*</label>
                <select name="pembawa" id="pembawa" class="form-control" required>
                    <option value="" disabled selected>PILIH PEMBAWA / DRIVER</option>
                    <?php foreach ($list_driver as $driver): ?>
                        <option value="<?php echo $driver->id; ?>"><?php echo $driver->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
              <label for="name">Jam Tiba*</label>
                <input class="form-control" required type="time" name="jam_tiba" id="jam_tiba" onfocusout="cekWaktu()"/>
            </div>
            <div class="form-group">
              <label for="name">Keterangan</label>
                <input class="form-control" type="text" name="keterangan" disabled/>
            </div>
            <div class="form-group">
              <label for="name">Dibuat Oleh ;</label>
                <?php echo $this->session->userdata['_name']; ?>
            </div>
            <input class="btn btn-success" type="submit" value="AJUKAN SK">
        </form>
    </div>
</div>

<div class="card-footer small text-muted">
	* Harus Diisi
</div>

<script>
  function getWarnaMobil() {
      var tipe_mobil = document.getElementById("tipe_mobil").value;
      var warna_mobil = $('#warna_mobil');
      $.ajax({
          type: "GET",
          url: '<?php echo site_url('tugas/warnaMobil')?>/'+tipe_mobil,
          success: function(data){
              warna_mobil.html(data);                
          }
      });
  }
  function cekNomorRangka() {
    var nomor_rangka = document.getElementById('nomor_rangka').value;
    if (nomor_rangka !== '') {
      $.ajax({
          type: "GET",
          url: '<?php echo site_url('skmasuk/ceknomorrangka')?>/'+nomor_rangka,
          success: function(data){
            if (data !== 'available') {
              alert('Nomor Rangka Sudah Terdaftar,Silahkan Gunakan Nomor Rangka Lain');
              document.getElementById('nomor_rangka').value = '';
            }
          }
      });
    }
  }
  function cekWaktu() {
        var jam_keluar = document.getElementById('jam_keluar').value;
        var jam_tiba = document.getElementById('jam_tiba').value;
        if (jam_keluar !== '') {
            if (jam_tiba > jam_keluar) {
                alert('Jam Kembali tidak boleh kurang dari Jam Keluar');
                document.getElementById('jam_keluar').value = '';
                document.getElementById('jam_tiba').value = '';
            }
        }
    }
</script>